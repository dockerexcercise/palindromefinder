package com.ivydemo.palindromefinder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PalindromefinderApplication {

	public static void main(String[] args) {

		SpringApplication.run(PalindromefinderApplication.class, args);
		System.out.println("Is '"+args[0]+"' a Palindrome? : " + isPalindrome(args[0]));
	}

	static boolean isPalindrome(String input)
	{

		int i = 0, j = input.length() - 1;
		while (i < j) {

			if (input.charAt(i) != input.charAt(j))
				return false;
			i++;
			j--;
		}

		return true;
	}

}
