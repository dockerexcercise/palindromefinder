FROM gradle:7.0.0-jdk8 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build
FROM openjdk:17-ea-18-jdk-oraclelinux8
COPY --from=build /home/gradle/src/build/libs/*.jar /palindromefinder.jar
ENTRYPOINT ["java","-jar","/palindromefinder.jar"]
CMD ["malayalam"]